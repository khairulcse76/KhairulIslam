-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 22, 2016 at 12:57 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `khairul`
--

-- --------------------------------------------------------

--
-- Table structure for table `student2`
--

CREATE TABLE IF NOT EXISTS `student2` (
`ID` int(11) NOT NULL,
  `Name` varchar(55) NOT NULL,
  `Email` varchar(55) NOT NULL,
  `password` varchar(20) NOT NULL,
  `phone` int(14) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student2`
--

INSERT INTO `student2` (`ID`, `Name`, `Email`, `password`, `phone`) VALUES
(1, 'Khairul Islam', 'khairulisalamtpi76@gmail.com', 'khairul', 1774460981),
(2, 'Khairul Islam', 'khairulisalamtpi76@gmail.com', 'khairul', 1774460981),
(3, '', '', '', 0),
(4, 'Manik', 'manik@gmail.com', '135454165521', 2147483647),
(5, 'Khairul Islam', 'sdfgwsdg@gmal.com', 'sdfsdfgsdfgsdfg', 0),
(6, 'Bangladesh', 'bangladesh@yahoo.com', 'pakistan', 0),
(7, '', '', '', 0),
(8, '', '', '', 0),
(9, '', '', '', 0),
(10, '', '', '', 0),
(11, 'bahgladesh', 'pakistan@yahoo.com', 'bangladesh', 0),
(12, '', '', '', 0),
(13, 'Khairul Islam', 'khairulislamtpi76@gmail.com', 'ertete4rt', 0),
(14, 'Khairul Islam', 'khairulislamtpi76@gmail.com', 'afasfa', 1784983612),
(15, 'Khairul Islam', 'khairulislamtpi76@gmail.com', 'adfasdf', 1767278406),
(16, '', '', '', 0),
(17, '', '', '', 0),
(18, '', '', '', 0),
(19, '', '', '', 0),
(20, 'dfghdf', 'khairulislamtpi76@gmail.com', 'adfasf', 2147483647),
(21, '', '', '', 0),
(22, '', '', '', 0),
(23, 'Khairul Islam', 'khairulislamtpi76@gmail.com', '12458568', 1738073362),
(24, '', '', '', 0),
(25, '', '', '', 0),
(26, '', '', '', 0),
(27, 'Mita', 'mita@gmail.com', 'mita', 1919407014),
(28, 'Mita', 'totapakhi@gmail.com', 'totapakhi', 1784986654),
(29, '', '', '', 0),
(30, '', '', '', 0),
(31, '', 'khairulislamtpi76@gmail.com', '', 0),
(32, 'Khairul Islam', 'khairulislamtpi76@gamil.com', '6131265654', 1784983612),
(33, 'bangladesh', 'Bahgladesh@gamil.com', 'bangladesh', 1798464685);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `student2`
--
ALTER TABLE `student2`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `student2`
--
ALTER TABLE `student2`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
