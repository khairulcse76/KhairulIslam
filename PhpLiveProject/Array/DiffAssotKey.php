<?php
echo "Array_Diff Function<br>";
$array_one=array(
    'a'=>'Red',
    'b'=>'black',
    'c'=>'Green',
    'd'=>'Blue'
);
$array_two=array(
    'a'=>'Red',
    'b'=>'black',
    'e'=>'Pink'
    
);
$array_three=array(
    'c'=>'Gray',
    'f'=>'black'
);

$diff=  array_diff($array_one, $array_two, $array_three);

echo "<pre>";
print_r($diff);
echo "</pre>";


echo "Array_Diff_assoc Function<br>";

$array_four=array(
    'a'=>'Red',
    'b'=>'black',
    'f'=>'Green',
    'd'=>'Blue'
);
$array_five=array(
    'e'=>'Red',
    'f'=>'black',
    'h'=>'Pink'
);
$array_six=array(
    'c'=>'Red',
    'f'=>'black',
    'h'=>'Pink'
);

$diffAssoc=  array_diff_assoc($array_four,$array_five, $array_six);

echo "<pre>";
print_r($diffAssoc);
echo "</pre>";


echo "Array_Diff_Key Function<br>";

$array_seven=array(
    'a'=>'Red',
    'b'=>'green',
    'c'=>'Yellow',
    'd'=>'Blue'
);
$array_eight=array(
    'a'=>'Red',
    'c'=>'black'
);

$array_Nine=array(
    'a'=>'Red',
    'j'=>'black',
    'b'=>'green',
    'f'=>'Yellow',
    'd'=>'Blue'
);

$diffKey=  array_diff_key($array_seven, $array_eight,$array_Nine);
echo "<pre>";
print_r($diffKey);
echo "</pre>";