<?php
session_start();

?>

<html>
    <head>
        <title>Image uploading System</title>
        
        <style>
            table{
                margin: 0 auto;
                border: 1px solid #3333ff;
                background: #cccccc;
                padding: 50px;
               
            }
            td{
                width: 100px;
                padding: 15px;
            }
            input[type="submit"]{
                background: #ff0000;
                color: white;
            }
        </style>
    </head>
    <body>
        <form action="store.php" method="post" enctype="multipart/form-data">
            <table>
                    <legend>insert A file/Image</legend>
                    <?php if(isset($_SESSION['store_msg']))
                    { ?>
                    <tr>
                        <td>
                            <?php echo $_SESSION['store_msg'];
                            unset($_SESSION['store_msg']);
                            ?>
                        </td>
                    </tr>
                   <?php }
                        ?>
                    <tr>
                        <td>Select your file<input type="file" name="image"></td>
                    </tr>
                    
                      <tr>
                          <td><input type="submit" name="submit" value="Upload"></td>
                    </tr>
            </table>
        </form>
    </body>
</html>