
<html>
    <head>
        <title>Live Project</title>
        <style>
            *{
    margin: 0 auto;
    padding: 0px;
}
.main{
    margin-top: 12px;
    width: 900px;
    height: 300px;;
    background-color: #cccccc;
}
.mainleft{
    border-right: 1px dotted #ff0000;
    margin: 50px 0px 0px 0px;
    float: left;
    width: 400px;
    
}
.mainrignt{
    float: right;
    width: 450px;
    margin: 50px 0px 0px 0px;
    
    
}
.tblone{
    width: 100%;
    border: 1px #ffffff;
    height: 30px;
}
.tblone td{
    padding: 5px 10px;
    text-align: left;
    margin-left: 10px;
}
.tblone th:hover{
    color: green;
}

input[type="text"]{
    border: 1px solid #ddd;
    margin-bottom: 5px;
    padding: 5px;
    width: 250px;
    font-size: 16px;
}

input[type="submit"]{
    cursor: pointer;
    float: right;
    margin-right: 60px;
    padding: 5px 15px;
    background: #666666
    
}
input[type="submit"]:hover{
    color: white;
    
}
a{
    text-decoration: none;
    padding: 5px 8px;
    background-color: #999999;
    color: #0000cc;
}
a:hover{
    color: #ffffff;
}
        </style>
       
    </head>
    <body>
         <?php include_once './H_header.php';?>
        <div class="main">

            <section class="mainleft">
                <form action="" method="post">
                     <table>
                    <tr>
                        <td>Name</td>
                         <td>:</td>
                         <td><input type="text" name="name" placeholder="Your Name"/></td>
                    </tr>

                     <tr>
                        <td>Department</td>
                         <td>:</td>
                         <td><input type="text" name="department" placeholder="Your Department"/></td>
                    </tr>
                     <tr>
                        <td>Age</td>
                         <td>:</td>
                         <td><input type="text" name="age" placeholder="Your Age"/></td>
                    </tr>

                    <tr>

                        <td colspan="3"><input type="submit" name="submit" placeholder="Your Name"/></td>
                    </tr>
                </table>
                </form>
            </section>

            <section class="mainrignt">
                <table class="tblone">
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Department</th>
                      <th>Age</th>
                      <th>Action</th>
                    </tr>

                     <tr>
                        <td>01</td>
                        <td>Khairul</td>
                        <td>Cse</td>
                        <td>23</td>
                        <td>
                            <a href="#">Edit</a>
                            <a href="#">Delete</a>
                        </td>
                    </tr>
                     <tr>
                        <td>02</td>
                        <td>Kamrul Islam</td>
                        <td>BBA</td>
                        <td>22</td>
                        <td>
                            <a href="#">Edit</a>
                            <a href="#">Delete</a>
                        </td>
                    </tr>

                     <tr>
                        <td>01</td>
                        <td>Maik</td>
                        <td>Math</td>
                        <td>23</td>
                        <td>
                            <a href="#">Edit</a>
                            <a href="#">Delete</a>
                        </td>
                    </tr>

                     <tr>
                        <td>01</td>
                        <td>Rasheduzzaman</td>
                        <td>EEE</td>
                        <td>25</td>
                        <td>
                            <a href="#">Edit</a>
                            <a href="#">Delete</a>
                        </td>
                    </tr>
                </table>
            </section>
        </div>
       <?php include '../../crud/view/footer.php';?>
    </body>
</html>


 
